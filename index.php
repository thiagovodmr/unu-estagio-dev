<!DOCTYPE html>
<html>
<head>
	<title>Sistema Acadêmico</title>
	<?php include_once "recursos.php"; ?>
	<style>
		body{
			text-align: center;
		}
		.btn{
			margin: auto;
			text-align: center;
			width: 80%;
		}
	</style>
</head>
<body>
	<div class="container">
			<h1>Sistema Acadêmico</h1>
		<a href="formulario_matricula.php">
			<button type="button" class="btn btn-primary btn-lg btn-block">Matricular Aluno</button>
		</a><br>
		<a href="formulario_aluno.php">
			<button type="button" class="btn btn-primary btn-lg btn-block">Cadastrar Alunos</button>
		</a><br>
		<a href="formulario_disciplina.php">
			<button type="button" class="btn btn-primary btn-lg btn-block">Cadastrar Disciplinas</button>
		</a><br>
		<a href="visualizar_alunos.php">
			<button type="button" class="btn btn-primary btn-lg btn-block">Listar Alunos</button>
		</a><br>
		<a href="visualizar_disciplinas.php">
			<button type="button" class="btn btn-primary btn-lg btn-block">Listar Disciplinas</button>
		</a><br>
	</div>
</body>
</html>