<?php 
include_once "../bd/conexao.php";

$id = $_GET["id"]; 

$sql = "DELETE from matriculas_disciplinas where id_disciplina = :id ";
$sql2 = "DELETE from notas where id_disciplina = :id";


$stmt = $conn->prepare($sql);
$stmt2 = $conn->prepare($sql2);

$stmt->bindParam(":id",$id);
$stmt2->bindParam(":id",$id);

$stmt->execute();
$stmt2->execute();

header("location: ../visualizar_alunos.php");

?>