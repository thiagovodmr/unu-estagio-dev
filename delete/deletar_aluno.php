<?php 
include "../bd/conexao.php";

$id = $_GET["id"];

$select = "SELECT id from matriculas where id_aluno = :id";
$query = $conn->prepare($select);
$query->bindParam(":id",$id);
$query->execute();
$row = $query->fetch(PDO::FETCH_ASSOC);

$id_matricula = $row["id"];

$sql1 = "DELETE from notas where id_matricula = :id_matricula";
$sql2 = "DELETE from matriculas_disciplinas where id_matricula = :id_matricula";
$sql3 = "DELETE from matriculas where id = :id_matricula";
$sql4 = "DELETE from alunos where id = :id";

$stmt1 = $conn->prepare($sql1);
$stmt2 = $conn->prepare($sql2);
$stmt3 = $conn->prepare($sql3);
$stmt4 = $conn->prepare($sql4);

$stmt1->bindParam(":id_matricula",$id_matricula);
$stmt2->bindParam(":id_matricula",$id_matricula);
$stmt3->bindParam(":id_matricula",$id_matricula);
$stmt4->bindParam(":id",$id);


$result1 = $stmt1->execute();
$result2 = $stmt2->execute();
$result3 = $stmt3->execute();
$result4 = $stmt4->execute();

if (! $result1 or !$result2 or !$result3 or !$result4){
	    var_dump( $stmt->errorInfo() );
	    exit;
}
else{
	header("location: /visualizar_alunos.php");
}  


?>