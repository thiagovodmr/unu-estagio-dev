<?php 

	include "../bd/conexao.php";



	$id = $_GET["id"];

	$sql = "DELETE from disciplinas where id = :id";
	$sql2 = "DELETE from matriculas_disciplinas where id_disciplina = :id";
	$sql3 = "DELETE from notas where id_disciplina = :id";

	$stmt = $conn->prepare($sql);
	$stmt2 = $conn->prepare($sql2);
	$stmt3 = $conn->prepare($sql3);

	$stmt->bindParam(":id",$id);
	$stmt2->bindParam(":id",$id);
	$stmt3->bindParam(":id",$id);

	$result = $stmt->execute();
	$result2 = $stmt2->execute();
	$result3 = $stmt3->execute();




	if (! $result  or !$result2 or !$result3){
	    var_dump( $stmt->errorInfo() );
	    var_dump($stmt2->errorInfo() );
	    var_dump($stmt3->errorInfo());
	    exit;
	}
	else{
		header("location: ../visualizar_disciplinas.php");
	}  



 ?>