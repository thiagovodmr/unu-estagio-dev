<?php 
	include "bd/conexao.php";

	$sql = "SELECT * FROM disciplinas order by nome";
	$stmt = $conn->query($sql);

	$aluno = $_GET["aluno"];
	$periodo = $_GET["periodo"];

	$sql2 = "SELECT id from matriculas where id_aluno = :aluno and periodo = :periodo";
	$stmt2 = $conn->prepare($sql2);
	$stmt2->bindParam(":aluno",$aluno);
	$stmt2->bindParam(":periodo",$periodo);
	$stmt2->execute();
	$row2 = $stmt2->fetch(PDO::FETCH_ASSOC); 
	$id = $row2["id"];
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php include_once "recursos.php"; ?>
	<style>
		form{
			font-size: 20px;
		}
	</style>
</head>
<body>
	<div class="container">
		<h1>Disciplinas</h1>
		<hr>
		<div class="form-group">
			<form action="bd/cadastrar_matric_discip.php?id=<?= $id ?>" method="POST">
				<?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
					<input type="checkbox" class="form-check-input" name="disciplina[]" value=<?= $row['id'] ?> >
					<?= $row["nome"] ?><br>
				<?php endwhile; ?>		
				<br>
				<hr>
				<button type="submit" class="btn btn-primary">Matricular</button>
			</form>
		</div>
	</div>
</body>
</html>