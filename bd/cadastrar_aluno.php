<?php 

	include_once "conexao.php";

	$nome = htmlspecialchars($_POST["nome"]);
	$data_nasc = $_POST["data_nasc"];

	$sql = "INSERT INTO alunos(nome,data_nasc) VALUES(:nome,:data_nasc)";
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":nome",$nome);
	$stmt->bindParam(":data_nasc",$data_nasc);
	$result = $stmt->execute();

	if (! $result ){
	    var_dump( $stmt->errorInfo() );
	    exit;
	}
	else{
		header("location: ../index.php");
	}  


 ?>