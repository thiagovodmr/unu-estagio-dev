<?php 

	include_once "conexao.php";


	$aluno = htmlspecialchars($_POST["aluno"]);
	
	if ($aluno != "null") {
		$periodo = htmlspecialchars($_POST["periodo"]);

		$sql = "INSERT INTO matriculas(id_aluno,periodo) VALUES (:aluno,:periodo)";
		
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(":aluno",$aluno);
		$stmt->bindParam(":periodo",$periodo);
		$result = $stmt->execute();

		if (! $result ){
		    var_dump( $stmt->errorInfo() );
		    exit;
		}
		else{
			header("location: ../formulario_matric_discip.php?aluno=$aluno&&periodo=$periodo");
		}  
	}
	else{
		header("location:../formulario_matricula.php");
	}

 ?>