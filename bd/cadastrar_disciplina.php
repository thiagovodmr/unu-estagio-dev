<?php 
	include_once "conexao.php";

	$nome = htmlspecialchars($_POST["nome"]);

	$sql = "INSERT INTO disciplinas(nome) VALUES(:nome)";

	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":nome",$nome);
	$result = $stmt->execute();

	if (! $result ){
	    var_dump( $stmt->errorInfo() );
	    exit;
	}
	else{
		header("location: ../index.php");
	}  
 ?>