<?php 
	include_once "bd/conexao.php";
	$sql = "SELECT * FROM alunos ORDER BY nome";
	$stmt = $conn->query($sql);
?>
<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<?php include_once "recursos.php"; ?>
		<style>
			.form-control{
			width: 60%;
			}
			.btn{
				width: 60%;
			}
			#voltar{
				width: 10%;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<h1>Matricula</h1>
			<div class="form-group">
				<form action="bd/cadastrar_matricula.php" method="POST">
					<label>Aluno: </label>
					<select class="form-control" name="aluno" required>
						<option value= "null">-</option>
						<?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
							<option value= <?= $row['id'] ?> >
								 <?=$row["nome"]?> 
							</option>
						<?php endwhile; ?>
					</select>
					<br>
					
					<label>Periodo: </label><br>
					<input class="form-control" type="number" name="periodo"  required><br><br>
					<button type="submit" class="btn btn-primary">proximo</button>
				</form>
			</div>
			<a href="index.php"><button id="voltar" class="btn btn-primary">voltar</button></a>
		</div>
	</body>
</html>