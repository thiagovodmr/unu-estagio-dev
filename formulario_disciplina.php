<!DOCTYPE html>
<html>
<head>
	<title>Cadastro da Disciplina</title>
	<?php include_once "recursos.php"; ?>
	<style>
		.form-control{
			width: 60%;
		}
		.btn{
			width: 60%;
		}
		#voltar{
			width: 10%;
		}
	</style>
</head>
<body>
	<div class="container">
		<h1>Cadastrar Disciplina</h1>	
		<div class="form-group">
			<form action="bd/cadastrar_disciplina.php" method="POST">
				<label >Nome:</label><br>
				<input class="form-control" type="text" name="nome"  required><br>
				<button type="submit" class="btn btn-primary">Cadastrar</button>
			</form>
		</div>
		<a href="index.php"><button id="voltar" class="btn btn-primary">voltar</button></a>
	</div>
</body>
</html>