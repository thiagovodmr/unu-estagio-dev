<?php 
	include_once "bd/conexao.php";

	$id = $_GET["id"];

	$sql = "
		SELECT m.id as matric_id,disc.id as disc_id,disc.nome,n.nota1,n.nota2,n.media from notas as n
		inner join disciplinas as disc on n.id_disciplina = disc.id
		inner join matriculas as m on m.id = n.id_matricula
		inner join alunos as al on m.id_aluno = al.id
		where al.id = :id
	 	";

	
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":id",$id);
	$resultados = $stmt->execute();
	
	$sql2 = "SELECT nome from alunos where id = :id";
	$num_rows = $stmt->rowCount();

	$stmt2 = $conn->prepare($sql2);
	$stmt2->bindParam(":id",$id);
	$stmt2->execute();
	$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

	$array = [];
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php  include_once "recursos.php"; ?>
	<style>
		body{
			text-align: center;
		}
		table,th,tr,td { border: 1px solid black; border-collapse: collapse; }
		td ,th{
 			width: 300px;
 			font-size: 20px;
 			text-align: center;
 		}
 		.btn{
	 		width: 400px;
	 	}
	 	.editar{
 			width: 150px;
 		}
 		i{
 			margin-right: 10px; 
 		}
	</style>
</head>
<body>
	<div class="container">
		<h1><?= $row2["nome"] ?></h1>
			<?php if ($num_rows == 0): ?>
				<div class="alert alert-info" role="alert">
						Este Aluno não está matriculado em nenhuma disciplina.
				</div>
				<?php 
					$sql = "DELETE from matriculas where id_aluno = :aluno";
					$stmt = $conn->prepare($sql);
					$stmt->bindParam(":aluno",$id);
					$stmt->execute();
				 ?>
						
			
			<?php else: ?>	
			
				<table>
					<tr>
						<th>N° Matricula</th>
						<th>Disciplina</th>
						<th>Nota 1</th>
						<th>Nota 2</th>
						<th>Media</th>
						<th class='editar'>
			 				<i class="fa fa-cog"></i>
			 			</th>
					</tr>
					
				<?php while($row=$stmt->fetch(PDO::FETCH_ASSOC)): ?>
					<tr>
						<td><?= $row["matric_id"] ?></td>
						<td><?= $row["nome"] ?></td>
						<td><?= $row["nota1"] ?></td>
						<td><?= $row["nota2"] ?></td>
						<td><?= $row["media"] ?></td>
						<td class="editar">
		 					<i onclick= "parametros(
		 					<?= $row['matric_id'] ?>,
		 					<?= $row["disc_id"] ?>,
		 					<?= $row['nota1'] ?>,
		 					<?= $row['nota2'] ?>
		 					)" class="fa fa-edit"></i>

		 					<a href="delete/deletar_matricula.php?id=<?= $row['disc_id'] ?>">
		 						<i class="fa fa-trash"></i>
		 					</a>
		 				</td>

						<?php array_push($array, FloatVal($row["media"])) ?>
					</tr>
				<?php endwhile; 
				
					$soma = 0;
					foreach ($array as $valor) {
						$soma+=$valor;
					}
					$media = $soma/ sizeof($array);
				?>
				<tr><td>Média Geral: </td><td><?= $media ?></td></tr>
				
			<?php endif ?>
		
		</table><br>
		<a href="visualizar_alunos.php"><button class="btn btn-primary">voltar</button></a>
	</div>

	<script>
			function parametros(matricula,disciplina,nota1, nota2){
				$('#modalTitle').html("Editar Matricula");

                $("#form").attr("action","update/editar_matricula.php?m="+matricula+"&&d="+disciplina+"&&id="+<?= $id ?>);

                $('#edit_Modal').modal();

                
			}

	</script>

	<div id="edit_Modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            
            <div class='modal-body'>
                    
                    <form id="form" role='form' method='POST'>
                      
                      <div class='form-group' ng-app="">

                        <label> Nota 1 </label>
                        <input id='Nota1' type="number" step = "any" class='form-control' ng-model="nota1" name="nota1" placeholder="digite a nova nota" required>

                        <label>Nota 2</label>
                        <input id="Nota2" type="number" step = "any" class="form-control" ng-model="nota2" name="nota2" placeholder="digite a nova nota" required>

                        <label>Média</label>
                        <input id="Media" type="number" step = "any" class="form-control" name="media" value="{{(nota1+nota2)/2}}" readonly="">
                      
                      </div>

                      <button  class='btn btn-primary' type="submit">
                          Alterar
                      </button>
                      
                    </form>
                </div>
        </div>
    </div>
  </div>

</body>
</html>