<?php 

	include_once "bd/conexao.php";


	$sql = "SELECT * FROM disciplinas";
	$stmt = $conn->query($sql);
	$num_rows = $stmt->rowCount();
 ?>

 <!DOCTYPE html>
 <html>
	 <head>
	 	<title></title>
	 	<?php include_once "recursos.php"; ?>
	 	<style>
	 		body{
	 			text-align: center;
	 		}
	 		table{
	 			margin: auto;
	 		}
	 		table,th,tr,td{
	 			border: 1px solid black;
	 			border-collapse: collapse;
	 		}
	 		td,th{
	 			width: 500px;
	 			font-size: 25px;
	 			text-align: center;
	 		}
	 		.btn{
	 			margin-top: 20px;
	 			width: 400px;
	 		}
	 		.editar{
	 			width: 150px;
	 		}
	 		i{
	 			margin-right: 10px; 
	 		}
	 	</style>
	 </head>
	 <body>
	 	<div class="container">
		 	<h1>Disciplinas</h1>
		 	
		 	<?php if($num_rows == 0): ?>
				<div class="alert alert-info" role="alert">
					  Nenhuma Disciplina Cadastrada
					</div>
			<?php else: ?> 			
			 	
			 	<table>
			 		<tr>
			 			<th>Nome</th>
			 			<th class='editar'>
			 				<i class="fa fa-cog"></i>
			 			</th>
			 		</tr>
			 		<?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
			 			<tr>
			 				<td>
			 					<a href="detalhes_disciplinas.php?id=<?= $row['id']?>">
			 						<?= $row["nome"] ?>
			 					</a>
			 				</td>
			 				<td class="editar">
			 					<i onclick="parametros(<?=$row['id']?>,'<?=$row["nome"]?>')" class="fa fa-edit"></i>
			 					<a href="delete/deletar_disciplina.php?id=<?= $row['id'] ?>">
			 						<i class="fa fa-trash"></i>
			 					</a>
			 				</td>
			 			</tr>
			 		<?php endwhile; ?>
			 	</table>
	 	
		 	<?php endif ?>	
	 	
	 	<a href="index.php"><button class="btn btn-primary">voltar</button></a>
	 	</div>

	 	<script>
			function parametros(id,nome){
				$('#modalTitle').html("Editar informações");
				$("#Nome").attr("value",nome);
				
				
                $("#form").attr("action","update/editar_disciplina.php?id="+id);

                $('#edit_Modal').modal();

                
			}
		</script>

	 	<div id="edit_Modal" class="modal fade">
    	<div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            
            <div class='modal-body'>
                    
                    <form id="form" role='form' method='POST'>
                      
                      <div class='form-group' ng-app="">

                        <label> Nome </label>
                        <input id='Nome' type="text" class='form-control' ng-model="nome" name="nome" required>
                      
                      </div>

                      <button  class='btn btn-primary' type="submit">
                          Alterar
                      </button>
                      
                    </form>
                </div>
		        </div>
		    </div>
		  </div>



	 </body>
 </html>