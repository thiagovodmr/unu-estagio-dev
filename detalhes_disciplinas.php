<?php 
	include_once "bd/conexao.php";

	$id = $_GET["id"];

	$sql = "
		SELECT al.nome, n.media from notas as n 
		inner join matriculas as m on n.id_matricula = m.id
		inner join alunos as al on al.id = m.id_aluno
		where n.id_disciplina = :id order by n.media desc
	 	";
	
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":id",$id);
	$stmt->execute();
	$num_rows = $stmt->rowCount();
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php include_once "recursos.php"; ?>
	<style>
		body{
			text-align: center;
		}
		table{
			margin: auto;
		}
		table,th,tr,td { border: 1px solid black; border-collapse: collapse; }
		td ,th{
 			width: 300px;
 			font-size: 20px;
 			text-align: center;
 		}
 		.btn{
	 		width: 400px;
	 	}
	</style>
</head>
<body>
	<div class="container">
		<h1>Alunos na Disciplina</h1>
		
		<?php if ($num_rows == 0): ?>
			<div class="alert alert-info" role="alert">
				Não tem aluno cursando esta disciplina.
			</div>

		<?php else: ?>
			<table>
				<tr><th>Aluno</th><th>Média</th></tr>
				<?php while($row=$stmt->fetch(PDO::FETCH_ASSOC)): ?>
					<tr>
						<td><?= $row["nome"]?></td>
						<td><?= $row["media"] ?></td>
					</tr>
				<?php endwhile; ?>
			</table>
		
		<?php endif ?>
		
		<br>
		<a href="visualizar_disciplinas.php"><button class="btn btn-primary">voltar</button></a>
	</div>
</body>
</html>