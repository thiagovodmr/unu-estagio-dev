<?php 
	include "../bd/conexao.php";
	
	$id = $_GET["id"];
	$nome = $_POST["nome"];
	$data_nasc = $_POST["data_nasc"];

	$sql = "UPDATE alunos SET nome = :nome, data_nasc = :data_nasc where id=:id";
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":nome",$nome);
	$stmt->bindParam(":data_nasc",$data_nasc);
	$stmt->bindParam(":id",$id);
	$stmt->execute();

	header("location: ../visualizar_alunos.php");



 ?>