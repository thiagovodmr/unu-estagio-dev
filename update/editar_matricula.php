<?php 
	include "../bd/conexao.php";
	$id = $_GET["id"];
	$m = $_GET["m"];
	$d = $_GET["d"];

	$nota1 = $_POST["nota1"];
	$nota2 = $_POST["nota2"];
	$media = $_POST["media"];

	$sql = "UPDATE notas set nota1 = :nota1, nota2 = :nota2, media = :media where id_matricula = :m and id_disciplina = :d";
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":nota1",$nota1);
	$stmt->bindParam(":nota2",$nota2);
	$stmt->bindParam(":media",$media);
	$stmt->bindParam(":m",$m);
	$stmt->bindParam(":d",$d);
	$result = $stmt->execute();

	if (! $result ){
	    var_dump( $stmt->errorInfo() );
	    exit;
	}
	else{
		header("location: ../detalhes_aluno.php?id=".$id);
	}  


?>