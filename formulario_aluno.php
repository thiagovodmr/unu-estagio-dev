<!DOCTYPE html>
<html>
<head>
	<title>Sistema Academico</title>
	<?php include_once "recursos.php"; ?>
	<style>
		.form-control{
			width: 60%;
		}
		.btn{
			width: 60%;
		}
		#voltar{
			width: 10%;
		}
	</style>
</head>
<body>

	<div class="container">
		<h1>Cadastrar Aluno</h1>
		<div class="form-group">
			<form action="/bd/cadastrar_aluno.php" method="POST">
				<label >Nome:</label><br>
				<input class="form-control" type="text" name="nome" required><br>
				<label>Data de nascimento</label><br>
				<input class="form-control" type="date" name="data_nasc"  required><br>
				<button type="submit" class="btn btn-primary">Cadastrar</button>
			</form>
		</div>
		<a href="index.php"><button id="voltar" class="btn btn-primary">voltar</button></a>
	</div>
</body>
</html>